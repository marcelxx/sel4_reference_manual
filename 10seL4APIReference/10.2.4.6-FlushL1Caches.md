#### 10.2.4.6  FlushL1Caches

LIBSEL4_INLINE_FUNC void seL4_BenchmarkFlushL1Caches

刷新硬件L1缓存。

类型 | 名字 | 描述
--- | --- | ---
seL4_Word | cache_type | 要刷新的缓存类型

*返回值*：无。

*描述*：刷新平台的L1缓存（目前仅支持ARM）。允许指定要刷新的缓存类型（如仅指令缓存，仅数据缓存，指令和数据缓存）。
